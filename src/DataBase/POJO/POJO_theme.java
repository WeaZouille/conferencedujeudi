package DataBase.POJO;

public class POJO_theme {

    private int idTheme;
    private String designationTheme;

    public POJO_theme() {
    }

    public POJO_theme(int _idTheme, String _designationTheme) {
        this.idTheme = _idTheme;
        this.designationTheme = _designationTheme;
    }
     public int getIdTheme() {
        return this.idTheme;
    }

    public void setIdTheme(int _idTheme) {
        this.idTheme = _idTheme;
    }
     public String getDesignationTheme() {
        return this.designationTheme;
    }

    public void setDesignationTheme(String _designationTheme) {
        this.designationTheme = _designationTheme;
    }

}
