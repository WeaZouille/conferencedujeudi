package DataBase.POJO;

public class POJO_conferencier {

    private int idConferencier;
    private String nomPrenomConferencier;
    private boolean conferencierInterne;
    private int idConference;
    private String blocNoteConferencier;

    public POJO_conferencier() {

    }

    public POJO_conferencier(int _idConferencier, String _nomPrenomConferencier, int _idConference) {
        this.idConferencier = _idConferencier;
        this.nomPrenomConferencier = _nomPrenomConferencier;
        this.idConference = _idConference;
    }

    public POJO_conferencier(int _idConferencier, String _nomPrenomConferencier, boolean _conferencierInterne, int _idConference, String _blocNoteConferencier) {
        this.idConferencier = _idConferencier;
        this.nomPrenomConferencier = _nomPrenomConferencier;
        this.conferencierInterne = _conferencierInterne;
        this.idConference = _idConference;
        this.blocNoteConferencier = _blocNoteConferencier;

    }

    public int getIdConferencier() {
        return this.idConferencier;
    }

    public void setIdConferencier(int _idConferencier) {
        this.idConferencier = _idConferencier;
    }

    public String getNomPrenomConferencier() {
        return this.nomPrenomConferencier;
    }

    public void setNomPrenomConferencier(String _nomPrenomConferencier) {
        this.nomPrenomConferencier = _nomPrenomConferencier;
    }

    public boolean isConferencierInterne() {
        return this.conferencierInterne;
    }

    public void setConferencierInterne(boolean _conferencierInterne) {
        this.conferencierInterne = _conferencierInterne;
    }

    public int getIdConference() {
        return this.idConference;
    }

    public void setIdConference(int _idConference) {
        this.idConference = _idConference;
    }

    public String getBlocNoteConferencier() {
        return this.blocNoteConferencier;
    }

    public void setBlocNoteConferencier(String _blocNoteConferencier) {
        this.blocNoteConferencier = _blocNoteConferencier;
    }
}
