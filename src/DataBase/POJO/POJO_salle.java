package DataBase.POJO;

public class POJO_salle {

    private int idSalle;
    private String nomSalle;
    private int nbPlaceSalle;

    public POJO_salle() {

    }

    public POJO_salle(int _idSalle, String _nomSalle, int _nbPlaceSalle) {
        this.idSalle = _idSalle;
        this.nomSalle = _nomSalle;
        this.nbPlaceSalle = _nbPlaceSalle;
    }
     public int getIdSalle() {
        return this.idSalle;
    }

    public void setIdSale(int _idSalle) {
        this.idSalle = _idSalle;
    }
     public String getNomSalle() {
        return this.nomSalle;
    }

    public void setNomSalle(String _nomSalle) {
        this.nomSalle = _nomSalle;
    }
     public int getNbPlaceSalle() {
        return this.nbPlaceSalle;
    }

    public void setNbPlaceSalle(int _nbPlaceSalle) {
        this.nbPlaceSalle = _nbPlaceSalle;
    }
}
