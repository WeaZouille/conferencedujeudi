package DataBase.POJO;

public class POJO_conferenceStatut {

    private int idConference;
    private int idStatut;

    public POJO_conferenceStatut() {

    }

    public POJO_conferenceStatut(int _idConference, int _idStatut) {
        this.idConference = _idConference;
        this.idStatut = _idStatut;
    }
     public int getIdConference() {
        return this.idConference;
    }

    public void setIdConference(int _idConference) {
        this.idConference = _idConference;
    }
     public int getIdStatut() {
        return this.idStatut;
    }

    public void setIdStatut(int _idStatut) {
        this.idStatut = _idStatut;
    }

}
