package DataBase.POJO;

class POJO_statut {

    private int idStatut;
    private String designationStatut;

    public POJO_statut() {

    }

    public POJO_statut(int _idStatut, String _designationStatut) {
        this.idStatut = _idStatut;
        this.designationStatut = _designationStatut;
    }

    public int getIdStatut() {
        return this.idStatut;
    }

    public void setIdStatut(int _idStatut) {
        this.idStatut = _idStatut;
    }
    public String getDesignationStatut() {
        return this.designationStatut;
    }

    public void setDesignationStatut(String _designationStatut) {
        this.designationStatut = _designationStatut;
    }
}
