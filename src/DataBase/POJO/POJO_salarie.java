package DataBase.POJO;

public class POJO_salarie {

    private int idSalarie;
    private String nomPrenomSalarie;

    public POJO_salarie() {

    }

    public POJO_salarie(int _idSalarie, String _nomPrenomSalarie) {
        this.idSalarie = _idSalarie;
        this.nomPrenomSalarie = _nomPrenomSalarie;
    }

    public int getIdSalarie() {
        return this.idSalarie;
    }

    public void setIdSalarie(int _idSalarie) {
        this.idSalarie = _idSalarie;
    }
     public String nomPrenomSalarie() {
        return this.nomPrenomSalarie;
    }

    public void setNomPrenomSalarie(String _nomPrenomSalarie) {
        this.nomPrenomSalarie = _nomPrenomSalarie;
    }
}
