package DataBase.POJO;

public class POJO_inscription {

    private int idInscription;
    private int idSalarie;
    private int idConference;

    public POJO_inscription() {

    }

    public POJO_inscription(int _idInscription, int _idSalarie, int _idConference) {
        this.idInscription = _idInscription;
        this.idSalarie = _idSalarie;
        this.idConference = _idConference;
    }

    public int getIdInscription() {
        return this.idInscription;
    }

    public void setIdInscription(int _idInscription) {
        this.idInscription = _idInscription;
    }

    public int getIdSalarie() {
        return this.idSalarie;
    }

    public void setIdSalarie(int _idSalarie) {
        this.idSalarie = _idSalarie;
    }

    public int getIdConference() {
        return this.idConference;
    }

    public void setIdConference(int _idConference) {
        this.idConference = _idConference;
    }
}
