package DataBase.POJO;

import java.util.Calendar;

public class POJO_inscriptionStatut {

    private int idInscription;
    private int idStatut;
    private Calendar dateStatutInscription;

    public POJO_inscriptionStatut() {

    }

    public POJO_inscriptionStatut(int _idInscription, int _idStatut, Calendar _dateStatueInscription) {
        this.idInscription = _idInscription;
        this.idStatut = _idStatut;
        this.dateStatutInscription = _dateStatueInscription;
    }

    public int getIdInscription() {
        return this.idInscription;
    }

    public void setIdInscription(int _idInscription) {
        this.idInscription = _idInscription;
    }
    public Calendar getDateStatutInscription() {
        return this.dateStatutInscription;
    }

    public void setDateStatutInscription(Calendar _dateStatutInscription) {
        this.dateStatutInscription = _dateStatutInscription;
    }
}
