package DataBase;

import java.awt.Button;
import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTable;
import java.awt.event.*;

public class CustomWindow extends JFrame {

    public CustomWindow() {
        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("Fichier");
        JMenu menu2 = new JMenu("Conférence");
        JMenuItem sousMenu = new JMenuItem("Fermer");
        this.setVisible(true);
        this.setTitle("Ma première fenêtre Java");
        this.setSize(1200, 1000);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setJMenuBar(menuBar);

        menu.add(sousMenu);

        menuBar.add(menu);

        menuBar.add(menu2);

        JLabel label = new JLabel("Bienvenue sur l'application");
        JPanel panel = new JPanel();
        this.add(panel);
        panel.add(label);

        JButton button = new JButton("CLICK&.....Suprise");
        panel.add(button);
        button.addActionListener((ActionListener) this);
    } 
}
