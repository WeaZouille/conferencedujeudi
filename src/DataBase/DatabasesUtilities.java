package DataBase;

import com.mysql.jdbc.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DatabasesUtilities {

    private static Statement stmt;
    private static ResultSet ResultSet;

    public static Connection getConnexion() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        Connection connection = null;
        try {
            connection = (Connection) DriverManager
                    .getConnection("jdbc:mysql://localhost:3306/conference", "root", "");
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return connection;
    }

    //méthode qui renvoie un type ResultSet
    public static ResultSet exec(String query) {
        Connection maconnexionB2D = getConnexion();
        Statement stmt = null;
        ResultSet resultatR = null;
        try {
            stmt = maconnexionB2D.createStatement();
        } catch (SQLException ex) {
            Logger.getLogger(DatabasesUtilities.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            resultatR = stmt.executeQuery(query);
        } catch (SQLException ex) {
            Logger.getLogger(DatabasesUtilities.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultatR;

    }
    //fin de la class DataBase
}
