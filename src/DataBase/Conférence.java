/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataBase;

import static DataBase.DatabasesUtilities.getConnexion;
import com.mysql.jdbc.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author benja
 */
public class Conférence {

    private int idConference;
    private String titreConference;
    private int dateConference;
    private int idConferencier;
    private int idSalle;
    private int idTheme;

    public Conférence() {
    }

    public Conférence(int _idConference, String _titreConference, int _idConferencier) {
        this.idConference = _idConference;
        this.titreConference = _titreConference;
        this.idConferencier = _idConferencier;
    }

    public Conférence(int _idConference, String _titreConference, int _idConferencier, int _idSalle, int _idTheme) {
        this.idConference = _idConference;
        this.titreConference = _titreConference;
        this.idConferencier = _idConferencier;
        this.idSalle = _idSalle;
        this.idTheme = _idTheme;
    }

    public int getidConference() {
        return this.idConference;
    }

    public void setName(int _idConference) {
        this.idConference = _idConference;
    }

    public String gettitreConference() {
        return this.titreConference;
    }

    public void settitreConference(String _titreConference) {
        this.titreConference = _titreConference;
    }

    public int getidConferencier() {
        return this.idConferencier;
    }

    public void setdateConference(int _idConferencier) {
        this.idConferencier = _idConferencier;
    }

    public int getidSalle() {
        return this.idSalle;
    }

    public void setidSalle(int _idSalle) {
        this.idSalle = _idSalle;
    }

    public int getidTheme() {
        return this.idTheme;
    }

    public void setidTheme(int _idTheme) {
        this.idTheme = _idTheme;
    }

    public void insert() {
        PreparedStatement ps = null;
        Connection _c = getConnexion();
        String query = "INSERT INTO conference"
                + "(titreConference,dateConference, idConferencier, idTheme)"
                + "VALUES (?, ?, ?, ?, ?)";
        try {
            ps = maconnectionB2D.prepareStatement(query);
            ps.setString(1, this.getidConference());
            ps.setDate(2, this.getSqlDate());
            ps.setInt(3, this.getIdConferencier());
            ps.setInt(4, this.getidTheme().getIdTheme());
            ps.setInt(5, this.getidSalle().getIdSalle());

            ps.execute();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
    }
}
